# gitlab-fork-sync

## About this branch

This branch contains GitLab CI job that syncs branches/tags between Git repositories,
most typically, from upstream repository to forked repository.

It syncs every add/update/delete of branches/tags in the source repository,
but it also preserves branches/tags with a specific prefix like `fork/` in the destination repository unchanged.

This is useful to maintain your local modification in branches/tags like `fork/patch-1` in the forked repository,
while keeping everything else outside `fork/` synced with the upstream repository.

It's recommended to run this CI job as a scheduled pipeline on your GitLab instance.
You can configure it with the following variables:

|Variable|Description|Examples|
|---|---|---|
|`REMOTE_SRC`|Remote for the source (upstream) repository.|`https://github.com/redmine/redmine`|
|`REMOTE_DST`|Remote for the destination (forked) repository.|`git@gitlab.com:yaegashi/redmime`<br/>(if empty, defaults to SSH transport access to the current repository)|
|`SSH_PRIVATE_KEY`|SSH private key to access private repositories.  Public key is typically registered as a "deploy key" in GitLab and/or GitHub.||
|`PREFIX`|Prefix for branches/tags to preserve in the destination repository.|`fork/` (default)<br/>`local-`|

If you put this branch in the destination repository,
make sure that this branch has a name with the prefix `$PREFIX`, like `fork/sync`.
Otherwise it will be removed when CI job runs.

## Set up

Assume that you already have a destination repository on GitLab instance, which is forked or imported from a source (upstream) repository.

First, install this `fork/sync` branch into the destination repository.

    git clone https://gitlab.com/yaegashi/gitlab-fork-sync
    cd gitlab-fork-sync
    git remote add dst <the-destination-repository-URL-to-push>
    git push dst fork/sync

Create a SSH private/public key with no passphrase:

    ssh-keygen -N '' -f ssh-key

This creates two text files: `ssh-key` (private key) and `ssh-key.pub` (public key).

Register the content of `ssh-key.pub` as a deploy key in the destination repository settings.  It should be write access allowed.

Create a scheduled pipeline in the destination repository CI/CD with the following settings:
- Set target branch to `fork/sync`
- Set `REMOTE_SRC` variable to the source repository URL to fetch
- Set `SSH_PRIVATE_KEY` to the content of `ssh-key`

Test it by manually running the scheduled pipline.

## To do

- [Git-LFS](https://git-lfs.github.com/) support
- Trigger GitLab CI pipeline using outgoing webhook from source repository
- Support other CI infrastructures than GitLab CI

## License

MIT