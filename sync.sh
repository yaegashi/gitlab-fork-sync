#!/bin/sh

set -e

if test -z "$REMOTE_SRC"; then
        echo "No REMOTE_SRC specified" >&2
        exit 1
fi

if test -z "$REMOTE_DST"; then
        if test -z "$CI_SERVER_HOST"; then
                # CI_SERVER_HOST is not available until 12.1
                CI_SERVER_HOST=${CI_PROJECT_URL#*//}
                CI_SERVER_HOST=${CI_SERVER_HOST%%/*}
        fi
        REMOTE_DST="git@$CI_SERVER_HOST:$CI_PROJECT_PATH"
fi

if test -n "$SSH_PRIVATE_KEY"; then
        echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - >/dev/null
fi

export GIT_SSH_COMMAND="ssh -o StrictHostKeyChecking=no"

git remote add dst $REMOTE_DST
git remote add src $REMOTE_SRC

set -x

# Fetch everything from dst repository for object caching
git fetch dst +refs/heads/*:refs/dst/heads/* --prune
git fetch dst +refs/tags/*:refs/dst/tags/* --prune

# Fetch everything from src repository
git fetch src +refs/heads/*:refs/src/heads/* --prune
git fetch src +refs/tags/*:refs/src/tags/* --prune

# Fetch $PREFIX-ed branches/tags from dst to local src
git fetch dst +refs/heads/${PREFIX}*:refs/src/heads/${PREFIX}*
git fetch dst +refs/tags/${PREFIX}*:refs/src/tags/${PREFIX}*

# Back up $PREFIX-ed branches/tags to refs/backup in dst repository
git push -o ci.skip dst +refs/src/heads/${PREFIX}*:refs/backup/heads/${PREFIX}*
git push -o ci.skip dst +refs/src/tags/${PREFIX}*:refs/backup/tags/${PREFIX}*

# Push everything in local src repository to dst repository
git push -o ci.skip dst +refs/src/heads/*:refs/heads/* --prune
git push -o ci.skip dst +refs/src/tags/*:refs/tags/* --prune
